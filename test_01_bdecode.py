# type: ignore

import zlib
import base64
import hashlib
from io import BytesIO
import unittest

from _01_bdecode import bdecode_next_val


class BencodeTest(unittest.TestCase):
    def test_get_torrent_file(self):
        # I have no idea what this torrent contains, I just entered "txt" on
        # ThePirateBay and looked for something small.
        sample_torrent_compressed = b"""
        eJydlDuS1DAQhhMOYjIIVjO2/BjrAMQES0QAstUzVo2sdkky88hJNqGKM3AJ0g25BFdBM2
        MbP4aqLSKX7a///rvVarFhXGtsdQk0ZZVzDVutnOHlHgzRh8aSg2VpnuarHgvpEPGgpHVK
        vTAQFE3m4EHqneKSlFiPuazntoZSgTWpKHUXiFEarsfkpifhiKK1RIA96fKKzpP/dYnGgH
        ZkZ8BKpYCg2d3V7F12z608zo3GrBVjsnBnRAIt24xdxtmduoVFU4PjquDOgTnNpKO8kw7X
        xHBT7EgNU1XaE7L5EvceScWN11IzB76jU6NSycJcCl+0aV7SGbW4gpv7je+6+dDzph0w0l
        SNrz7u0ULutoguzOOIWNj7SHnPQryYpwKsT3BLZIkGtwgZcgy5e7wWd6dhXqbDPWhicFpm
        lHQYNv6vgBq1vE6rx7xINBMpsWnA2NZsL4r7KxSvF+NUQSMNd1Dw03z2wk4x7w79kirKpj
        Of/stTSGk2Bhf2ytLzs4NM5pNhHfo7Id3iwKdc0xZKloXrujE9jCl6MVpI19+7RcCLPdB5
        Kwfw/5wuN9eYHZdEO81uIQ237f5egoz5j7UvNg3Z463sQOBBK+QCRLA1WAddO4KSlxUE3A
        WdFTkMup+NcM1KA35WRFCc/OptO7VVmCVr/379KVEHwjMyDPMkz9LMz92GgS5R+P2asA+P
        7x42MZN6iyJlCvTOVTKhCcRM8xp8+98jqsCCV3M2eNOgL84FhV+Okhth3xJ3dGHEGgklBF
        18miQ0hfT20UZr9vHp28/Xv88/8NUn9vn7s3j+9fQV4A/z+0UM"""

        sample_torrent_bin = base64.b64decode(sample_torrent_compressed)
        sample_torrent = zlib.decompress(sample_torrent_bin)

        infohash_obj = hashlib.sha1()
        d = bdecode_next_val(BytesIO(sample_torrent).read, infohash_obj)

        self.assertEqual(d[b'info'][b'length'], 535)
        self.assertEqual(infohash_obj.hexdigest(),
                         "37d0c2c8ae3c1465f3d8761ac7029a58eda7fe42")
