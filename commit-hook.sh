#!/bin/bash

set -euo pipefail

find . -type f -name '*.py' -exec python3 -m flake8 {} +
find . -type f -name '*.py' -exec python3 -m pylint --ignored-modules=boto3 -E {} +
find . -type f -name '*.py' -exec python3 -m mypy --strict {} +

# is here because unittest won't like names like ./a.py instead of a.py.
find * -name 'test_*.py' -exec coverage run -a --branch -m unittest {} \;
coverage report
