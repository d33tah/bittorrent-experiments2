#!/usr/bin/env python

from io import BytesIO
from typing import Any, Dict, List, Union, Optional, Callable


T_HASH = Any
T_BENCODED_DICT = Dict[
    bytes, Union[
        bytes, List[List[bytes]], int, Dict[bytes, Union[int, bytes]]
    ]
]
T_BENCODED_LIST = Any
T_READLIKE_F = Callable[[Optional[int]], bytes]


def _read_from_file(
            f: T_READLIKE_F,
            n: int,
            infohash_obj: T_HASH,
            _is_info: bool
        ) -> bytes:
    ret = f(n)
    if _is_info and infohash_obj:
        infohash_obj.update(ret)
    return ret


def _read_number_until(
            f: T_READLIKE_F,
            c: bytes,
            infohash_obj: T_HASH,
            _is_info: bool
        ) -> bytes:
    ret = b""
    while True:
        v = _read_from_file(f, 1, infohash_obj, _is_info)
        if v.isdigit() or v == b'-':
            ret += v
        else:
            if v != c:
                err = "ERROR: Expected %r, got %r." % (c, v)
                raise ValueError(err)
            return ret


def _bdecode_dict(
        f: T_READLIKE_F,
        infohash_obj: T_HASH,
        _is_info: bool
        ) -> T_BENCODED_DICT:
    ret: T_BENCODED_DICT = {}
    while True:
        key = bdecode_next_val(f, infohash_obj, _is_info)
        if key is None:
            return ret
        is_info_local = True if key == b'info' else _is_info
        value = bdecode_next_val(f, infohash_obj, is_info_local)
        ret[key] = value
    return ret


def _bdecode_list(
            f: T_READLIKE_F,
            infohash_obj: T_HASH,
            _is_info: bool
        ) -> T_BENCODED_LIST:
    ret: T_BENCODED_LIST = []
    while True:
        v = bdecode_next_val(f, infohash_obj, _is_info)
        if v is not None:
            ret += [v]
        else:
            return ret


def bdecode_next_val(
            f: T_READLIKE_F,
            infohash_obj: Optional[T_HASH] = None,
            _is_info: bool = False
        ) -> Any:
    t = _read_from_file(f, 1, infohash_obj, _is_info)
    if t == b'e':
        return None
    elif t == b'd':
        return _bdecode_dict(f, infohash_obj, _is_info)
    elif t.isdigit():
        t += _read_number_until(f, b":", infohash_obj, _is_info)
        ret = _read_from_file(f, int(t), infohash_obj, _is_info)
        return ret
    elif t == b'l':
        return _bdecode_list(f, infohash_obj, _is_info)
    elif t == b'i':
        return int(_read_number_until(f, b'e', infohash_obj, _is_info))
    else:
        raise ValueError("Unexpected type: %s" % repr(t))


def bdecode_str(s: bytes, infohash_obj: T_HASH = None) -> Any:
    b = BytesIO(s)
    return bdecode_next_val(b.read, infohash_obj)
